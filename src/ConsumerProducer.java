import java.time.Duration;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ConsumerProducer {



    private static final Random generator = new Random();
    private static final Queue<String> queue = new LinkedList<>();

    public static void main(String[] args) {
        int itemCount = 5;

        Thread producer = new Thread(() -> {
            for (int i = 0; i < itemCount; i++) {
                try {
                    Thread.sleep(Duration.ofSeconds(generator.nextInt(5)).toMillis());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (queue) {
                    queue.add("Dodano produkt nr : " + i);
                    queue.notify();
                }
            }
        });

        Thread consumer = new Thread(() -> {
            int itemsLeft = itemCount;
            int i =0;
            while (itemsLeft > 0) {
                String item;
                synchronized (queue) {
                    if (queue.isEmpty()) {
                        i++;
                        System.out.println("KOLEJNY OBROT" + i);
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    item = queue.poll();
                }
                itemsLeft--;
                System.out.println("Zabrano z kolejki produkt nr : " + item);
            }
        });

//
//        consumer.start();
//        producer.start();
    }
}