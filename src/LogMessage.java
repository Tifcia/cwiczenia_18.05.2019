import java.util.logging.Level;
import java.util.logging.Logger;

public class LogMessage implements MessagePrintable{
    Logger logger = Logger.getLogger(LogMessage.class.getName());

    public void logSomething(){
        logger.log(Level.WARNING, "Uwaga uwaga");
    }

    @Override
    public void printMessage (){
        logger.log(Level.WARNING, "Nadpisana metoda");
    }
}
