import java.util.logging.Logger;

public class Main {
    public static void main(String[] arg) {
        Formula formula = new Formula() {
            @Override
            public double calculate(int a) {
                return sqrt(a * 100);
            }
        };
        formula.calculate(100);
        formula.sqrt(16);

        Student s = new Student("Imie", 15);
        s.printMessage();

        Prowadzacy p = new Prowadzacy("Imie Prowadzącego", 40);
        p.printMessage();
    }
}
