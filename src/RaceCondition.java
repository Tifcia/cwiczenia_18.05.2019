public class RaceCondition {
    public static void main (String[]args) throws InterruptedException {
            ExampleWorker r = new ExampleWorker();

            Thread t1 = new Thread(r);
            Thread t2 = new Thread(r);
            Thread t3 = new Thread(r);

            t1.start();
            t2.start();
            t3.start();

            t1.join();
            t2.join();
            t3.join();

            System.out.println(r.c.getValue());
        }
    }