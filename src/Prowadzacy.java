import java.util.logging.Level;
import java.util.logging.Logger;

public class Prowadzacy implements MessagePrintable{

    String name;
    int wiek;

    public Prowadzacy(String name, int wiek) {
        this.name = name;
        this.wiek = wiek;
        this.logger = logger;
    }

    Logger logger = Logger.getLogger(Student.class.getName());
    @Override
    public void printMessage (){
        logger.log(Level.WARNING, "Nadpisana metoda u Prowadzącego");
    }

}
