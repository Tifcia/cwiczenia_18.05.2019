public class Counter {

    private int value;
    public void increment1(){
        synchronized (this){
            value +=1;
        }
    }
    public synchronized void increment() {
        value += 1;
    }

    public int getValue() {
        return value;
    }

}
