public class ExampleWorker implements Runnable {

    Counter c;

    public ExampleWorker() {
        c = new Counter();
    }

    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            c.increment();
        }
    }
}
